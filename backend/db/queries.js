var knex = require('./knex');

function Products() {
    return knex('products');
}

// *** queries *** //

function getAll() {
    return Products().select();
}


module.exports = {
    getAll: getAll
};
import React, { Component } from 'react';
import ProductList from './components/ProductList';
import Cart from './components/Cart'
import './App.css'



class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            products: [],
            cart: []
        }
    }

    componentDidMount() {

        fetch('/products')
            .then(res=> res.json())
            .then(products => this.setState({ products }));
    }


    handleClick (e) {
        e.preventDefault();

        this.state.cart.push(e.target["name"])
        this.setState({
            cart: this.state.cart
        })
    }

    displayItem () {
     return this.state.cart

       }

  render() {


    return (
        <div>
        <h1 className="center-align header-title"> Sabzi For Good (SFG)</h1>
      <div className="flexItem cart-item">  </div>
      <div className="App flexContainer ">

          <div className="flexItem product-list center-align">
              <ProductList onClickProduct={(e)=> this.handleClick(e)} products = {this.state.products} />
      </div>
        <ul>
      <Cart allItems={this.displayItem()}/>
        </ul>
          </div>
        </div>

    );
  }
}

export default App;

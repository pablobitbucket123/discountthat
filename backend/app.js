const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const queries = require('./db/queries');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.get('/', (req, res) => res.send('Hello World!'));

app.get('/products', (req,res, next)=> {
    queries.getAll()
        .then((products) => {
            res.status(200).json(products);
        })
        .catch((error)=>{
            next(error);
        });
});

app.listen(3001, () => console.log('Example app listening on port 3001!'))


module.exports = app

# Discount That

Tech test : 

One of the budding wholesale retailers in Capitalville, Mr. Rich Fairsell, has been looking at ways of boosting the sale numbers for his new online grocery store, Sabzi For Good (SFG). SFG's inventory has around 1MM products with prices mentioned inclusive of 20% VAT.
					
He decided to run a marketing campaign sending out limited time unique discount codes to all ~50MM citizens of Capitalville. The customers will be able to put in the discount code on the checkout page and get 40% off.


## Tech stack and structure 

A backend folder and a frontend folder have been created.

In the backend folder : Node, express and Postgresql are used to build the API.
chai, chai-http, and mocha are used to test the backend. 

In the frontend folder, we  use ReactJS to leverage the API endpoints built by the backend. The frontend
was created with the facebook create-react app utility which provides the main useful dependency for React. 

### Environment Setup


Install React front end dependencies

```
cd frontend
npm install
```

Install Express backend dependencies

```
cd backend
npm install
```

Also in the backend we need to install knex globally. This is required to set up knex.js with Postgres. We will then be able to configure the DB environments and create seed files.

```
npm install knex -g
```

Then we need to go your local postgres Database and create the following  :

```
psql
CREATE DATABASE discount_that;
CREATE DATABASE discount_that_test;
```

While we are still in the backend folder (in Terminal), we can migrate the tables columns of our respective DB, and also we can seed the files with sample data.

```
knex migrate:latest --env development
knex migrate:latest --env test
knex seed:run --env development
knex seed:run --env test
```

In the postgresql database, the table 'products' should be seeded with the sampple data located under backend/data/products.js

![](https://content.screencast.com/users/Pablo_Zendesk/folders/Jing/media/852dcce2-e1d6-47fb-9899-926701d84c48/00000027.png)

### Getting started

The backend needs to be launched first 

```
cd backend
nodemon app
```

The backend express server will be launched for port 3001.

Then the front end needs to be launched next : 

```
cd frontend
npm start
```

This will launch the React app who will use the JSON data served 
by the backend express app. 


### Testing 

To test the backend, mocha needs to be run from the backend root folder: 

```
cd backend
mocha
```

I have tried several ways to test the asynchronous call made via componentDidMount from the App component, 
but I have not managed to use jest and other testing dependencies to find out if the data are correctly 
retrieved via the API. 

```
cd frontend
npm test
```

The frontend tests are currently on red. 



### Challenges   

I tried using mongoDB but I was not able to extract the relevant data from the database, so I switched to postgresql which 
allowed me to build a simple table which can be seeded with commands provided by knex.js.

I spent a long time trying to find out how to test asynchronous calls on my react component which is doing a simple fetch on the database, but 
I could not figure out how to mock it. Possible that a dependency like sinon or fetchmock could have helped me get the correct test. 

 
### What I would have liked to add 

My intention was to build a simple shopping cart with persistent data. In the end all I have is just a page displaying data 
that come from the database.

I have added an add to cart button for each item. The intention was to display each bought item on the page. The next step would 
have been to add a checkout button when the user is done with online shopping. This checkout button could have sent a  post request via fetch
to the database in order to update the stock for all the items selected by the user. 














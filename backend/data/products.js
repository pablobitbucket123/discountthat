module.exports = [
    {
        name: "product1",
        stock: 111111,
        imageUrl: "https://content.screencast.com/users/Pablo_Zendesk/folders/Jing/media/7927bb7a-c9d7-4dec-ba87-66454e9eeab2/00000029.png",
        price: 45
},
{
    name: "product2",
    stock: 22222,
    imageUrl: "https://content.screencast.com/users/Pablo_Zendesk/folders/Jing/media/7f4d95b9-5964-4c10-a56c-011857dea8fb/00000030.png",
    price: 20
},
{
    name: "product3",
    stock: 33333,
    imageUrl: "https://content.screencast.com/users/Pablo_Zendesk/folders/Jing/media/af23a9c2-95ca-497c-98a7-9c321ac56ae8/00000031.png",
    price: 31
},
{
    name: "product4",
    stock: 44444,
    imageUrl: "https://content.screencast.com/users/Pablo_Zendesk/folders/Jing/media/d2b5d827-9d30-4def-bc9b-8c639351f573/00000032.png",
    price: 18
},
{
    name: "product5",
    stock: 555555,
    imageUrl: "https://content.screencast.com/users/Pablo_Zendesk/folders/Jing/media/d015bcb1-1dc2-43c1-be47-1d9818b7300a/00000033.png",
    price: 42
}
];
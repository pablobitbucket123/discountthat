
exports.up = function(knex, Promise) {
    return knex.schema.createTable('products', function(table){
        table.increments();
        table.string('name').notNullable().unique();
        table.string('imageUrl').notNullable();
        table.integer('price').notNullable();
        table.integer('stock').notNullable();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('products');

};

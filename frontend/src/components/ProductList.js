import React, { Component } from 'react';


class ProductList extends Component {

    constructor (props) {
        super(props)
        this.state = {

        }


    }

//     handleClick(e) {
//         console.log(e.target)
// }



    render() {



        return (
            <div className="App">

                {this.props.products.map(product =>

                <div key={product.id} className="row">
                    <div className="col s12 m7">
                    <div className="card">
                    <div key={product.id} className="card-image">
                    <img src={product.imageUrl}/>
                    <span className="card-title">{product.name}</span>
                </div>
                <div className="card-content left-align">

                </div>
                <div>   <p>{product.name}</p>  Price : £{product.price}</div>
                <div className="card-action center-align">

                    <div > <a  onClick={(e) => this.props.onClickProduct( e)} href="#"   key={product.name} className="card-action center-align" name={product.name}> Add to Cart</a></div>
                </div>

                </div>
                </div>
                </div>

                )}
            </div>
        );
    }
}

export default ProductList;

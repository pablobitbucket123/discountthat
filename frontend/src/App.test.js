import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {shallow} from 'enzyme';


// it('renders without crashing', () => {
//   const div = document.createElement('div');
//   ReactDOM.render(<App />, div);
//   ReactDOM.unmountComponentAtNode(div);
// });

describe('</Component', ()=>{

    const DELAY_MS = 8000

    const sleep = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }


    const fetchResponseJson = async (url) => {
        try {
            const response = await fetch(url)
            const responseJson = await response.json()
            await sleep(DELAY_MS)
            return responseJson
        }
        catch (e) {
            console.log(`fetchResponseJson failed:`, e)
        }
    }

    test(`fails with synchronous code`, () => {
        const responseJson = fetchResponseJson(`http://localhost:3001/products`)
        expect(responseJson).not.toHaveProperty(`name`, `price`)
    });

    // test('using promises', ()=>{
    //     expect.assertions(1)
    //     return fetchResponseJson( "http://localhost:3001/products")
    //         .then((responseJson)=>{
    //             expect(responseJson).toHaveProperty('name','price');
    //         })
    // });

    test('should contain the .app class', ()=>{
      const wrapper = shallow(<App/>);
      console.log(wrapper, "**********")
      expect(Object.values(wrapper.find('.app'))).toEqual(1);
    })


});



